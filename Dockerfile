FROM node:carbon
WORKDIR /usr/src/app 

RUN apt-get install git-core 
RUN git clone https://VictorSierra@bitbucket.org/VictorSierra/api_siee.git
WORKDIR /usr/src/app/api_siee
#COPY . .
RUN mkdir uploads
RUN npm install
CMD [ "npm", "start" ]