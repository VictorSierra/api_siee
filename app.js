'use strict'
let express = require('express');
let bodyParser = require('body-parser');

let app = express();

//carga rutas
let user_routes = require('./routes/user');
let casilla_router=require('./routes/casillas');
let acta_router=require('./routes/actas');
let catalogos_router=require('./routes/catalogos');

//app.use(bodyParser.urlencoded({ extended: false }));
//app.use(bodyParser.json());

app.use(bodyParser.urlencoded({limit: '50mb', extended: true}));
app.use(bodyParser.json({limit: '50mb'}));


//configuracion de cabeceras http   
app.use((req, res, next) => {
    const origin=req.get('origin');
    res.header('Access-Control-Allow-Origin', origin);
    res.header("Access-Control-Allow-Credentials", true);
    res.header('Access-Control-Allow-Headers', 'authorization,X-API-KEY,Origin,X-Requested-With,Content-Type,Accept,Access-Control-Allow-Request-Method');
    res.header('Access-Control-Allow-Methods', 'GET,POST,OPTIONS,PUT,DELETE');
    res.header('Allow', 'GET', 'POST', 'OPTIONS', 'PUT', 'DELETE');
    res.header()
    next();
});
//rutas base
app.use('/api', user_routes);
app.use('/api', casilla_router);
app.use('/api', acta_router);
app.use('/api', catalogos_router);
module.exports = app;