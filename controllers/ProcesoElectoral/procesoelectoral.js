'use strict'
let ProcesoElectoral=require('../../models/Eleccion/ProcesoElectoral.model');
function altaProcesoElectoral(req,res){
    let params = req.body;
    let procesoelectoral=params.procesoelectoral;
    let tipoProcesoelectoralid=params.tipoProcesoelectoralid;
    let nombreTipoProcesoElectoral=params.nombreProcesoElectoral;
    let abreviaturaTipoProcesoElectoral=params.abreviaturaTipoProcesoElectoral;
    let fechaJornada=params.fechaJornada;
    let descripcion=params.descripcion;
    let fechaCierre=params.fechaCierre;
    console.log('parametros');
    console.log(params);
    let proceso= new ProcesoElectoral();
    proceso.ProcesoElectoralID=procesoelectoral;
    proceso.TipoProcesoElectoral=nombreTipoProcesoElectoral;
    proceso.TipoProcesoElectoralAbreviacion=abreviaturaTipoProcesoElectoral;
    proceso.TipoProcesoElectoral_ID=tipoProcesoelectoralid;
    proceso.FechaJornada=fechaJornada;
    proceso.FechaCierre=fechaCierre;
    proceso.DescripcionJornada=descripcion;
    proceso.Activo=true;
    proceso.save(function(error,guardado){
        if(error){
            res.status(500).send({message:'Error al procesar la petición de alta del proceso electoral'});
        }else{
            if(!guardado){
                res.status(400).send({message:'No se logro guardar el proceso electoral'})
            }else{
                res.status(200).send({proceso:guardado});
            }
        }
    });
}

function procesoElectoralActual(req,res){

}


module.exports={
    altaProcesoElectoral
}