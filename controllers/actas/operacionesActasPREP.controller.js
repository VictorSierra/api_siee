'use strict'
    let fs = require('fs');
let path = require('path');
let Casillas =require('../../models/Casillas/casillas.model');
let Actas= require('../../models/Actas/acta.model');

function obtenerActa(req,res) {
    let params= req.body;
   let idActa=params.idActa;
   Casillas.findOne({Actas:{idActa : idActa}},(error,ActaR)=>{
        if(error){
            return res.status(404).send({message:'Hubo un problema en la petición del acta', error:error});
        }else{
            if(!ActaR){
                return res.status(404).send({message:'Acta no encontrada'});
            }else{
                return res.status(200).send({acta:ActaR});
           }
       }
   });
}

function obtenerImagenActa(req,res){
    //crear procedimiento para obtener las imagenes dadas de alta  
        console.log(req.params);
        let imagen_file = req.params.imagefile;
        let path_file = '/' + imagen_file;
        if (path_file.length > 0) {
            console.log(path_file);
            fs.exists(path_file, function(exists) {
                console.log(exists);
                if (exists) {
                    res.sendFile(path.resolve(path_file));
                } else {
                    res.status(200).send({ message: 'No existe la imagen' });
                }
            });
        } else {
            res.status(200).send({ message: 'no tiene una imagen asociada' });
        }    
    }
//verificar si se agregan esta no esta funcional
function AgregarActaDesdeSIEE(req,res){
    let params = req.body;
    let ProcesoElectoral=params.ProcesoElectoral;
    let UsuarioAsignado=params.UsuarioAsignado;
    let Acta=params.Acta;
    let idCasilla= params.idCasilla;    
    let casilla=Casillas.findById(idCasilla);
    let acta= new Actas();
    acta.Id_Casilla=casilla._id;
    acta.Entidad_Federativa=Acta.Entidad_Federativa;
    acta.Distrito_Electoral=Acta.Distrito_Electoral;
    acta.Seccion_Electoral=Acta.Seccion_Electoral;
    acta.Tipo_Eleccion=Acta.Tipo_Eleccion;
    acta.Hora_Envio=Acta.Hora_Envio;
    acta.Fecha_Envio=Acta.Fecha_Envio;
    acta.Nombre_Archivo_Enviado=Acta.Nombre_Archivo_Enviado;
    acta.Nombre_Archivo_Guardado=Acta.Nombre_Archivo_Guardado;
    acta.Hash_Recibido=Acta.Hash_Recibido;
    acta.Hash_generado=Acta.Hash_generado;
    acta.Detalle_Archivo.Extension=Acta.Detalle_Archivo.Extension;
    acta.Detalle_Archivo.Tam_Archivo=Acta.Detalle_Archivo.Tam_Archivo;
    acta.Ubicacion.latitud=Acta.Ubicacion.latitud;
    acta.Ubicacion.longitud=Acta.Ubicacion.longitud;
    acta.save((error,Agregada)=>{
        if(error){
            return res.status(500).send({message:'Hubo un problema en la petición de la casilla', error:error});        
        }else{
            if(!Agregada){
                return res.status(404).send({message:'El acta no se agrego'});
            }else{
                casilla.Actas.push({                    id_acta:Agregada._id                });
                casilla.save((error,casillaActualizada)=>{
                    if(error){
                        return res.status(500).send({message:'Error al vincular con la el acta con la casilla'});
                    }else{
                        if(!casillaActualizada){
                            return res.status(404).send({message:'No se encontro la casilla', acta:Agregada});
                        }else{
                            return res.status(200).send({Casilla:casillaActualizada,acta:Agregada});
                        }
                    }
                });
            }
        }
    });
        
                       
}
function EnviarActaalSIEE(datos){
    console.log(datos);
    //file
    //Eleccion_ID
    //ProcesoElectoral_ID
    //Fecha_Acopio
    //Hora_Acopio
    //Usuario_ID
    //Hash   
    return "" 
}

module.exports = {
    obtenerActa,
    AgregarActaDesdeSIEE,
    obtenerImagenActa,
    EnviarActaalSIEE
}