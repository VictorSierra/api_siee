'use strict'
let Casillas =require('../../models/Casillas/casillas.model');

function altaCasilla(req,res) {
    let params= req.body;
    //añadir usuario quien modifica
    //fecha hora alta 
    let casilla= params.casilla;    
    let NuevaCasilla= new Casillas();   
    NuevaCasilla.ProcesoElectoral= casilla.ProcesoElectoral;
    NuevaCasilla.IdentificacionCasilla.Entidad_Federativa= casilla.IdentificacionCasilla.Entidad_Federativa;
    NuevaCasilla.IdentificacionCasilla.Distrito_Electoral= casilla.IdentificacionCasilla.Distrito_Electoral;
    NuevaCasilla.IdentificacionCasilla.Seccion_Electoral= casilla.IdentificacionCasilla.Seccion_Electoral;
    NuevaCasilla.IdentificacionCasilla.Tipo_Casilla= casilla.IdentificacionCasilla.Tipo_Casilla;
    NuevaCasilla.IdentificacionCasilla.Numero_Casilla= casilla.IdentificacionCasilla.Numero_Casilla;
    NuevaCasilla.IdentificacionCasilla.Municipio_Alcaldia= casilla.IdentificacionCasilla.Municipio_Alcaldia;
    NuevaCasilla.Identificacion_SIEE= casilla.Identificacion_SIEE;
    NuevaCasilla.UsuarioAsignado= casilla.UsuarioAsignado;
    NuevaCasilla.Fecha_Hora_Alta= casilla.Fecha_Hora_Alta;
    NuevaCasilla.Actas=[];
    //NuevaCasilla.Actas= casilla.Actas;//en el caso que las actas se agreguen desde SIEE
    console.log(NuevaCasilla);
    NuevaCasilla.save( (error,casillaAdd)=> {
        if(error){
            return res.status(404).send({message:'Ocurrio un problema al procesar la petición de agregar la casilla', casilla:casilla});
        }else{
            if(!casillaAdd){
                return res.status(404).send({message:'No se logro insertar la casilla'});
            }else{
                console.log(casillaAdd);
                return res.status(202).send({
                    casilla:casillaAdd
                });
            }
        }
    });
}
function asignaCasillaUsuarioAPP(req, res) {
    let params=req.body;
    let ProcesoElectoral=params.ProcesoElectoral;
    let Identificacion_SIEE= params.Identificacion_SIEE;
    let UsuarioAsignado=params.UsuarioAsignado;
    let NuevoUsuario= params.NuevoUsuario;
    console.log(params);
    Casillas.findOne({
        Identificacion_SIEE:Identificacion_SIEE,
        ProcesoElectoral:ProcesoElectoral
    },
    (error,casilla)=>{
        if(error){
            return res.status(500).send({message:'Ocurrio un error al procesar la petición'});
        }else{
                if(!casilla){
                    return res.status(404).send({message:'No se encontro la casilla'});
                }else{
                    //casilla.UsuarioAsignado=NuevoUsuario;
                    console.log(casilla);
                    Casillas.findByIdAndUpdate( casilla._id,{UsuarioAsignado:NuevoUsuario},(error,actualizado)=>{
                        if(error){
                            return res.status(500).send({message:'Ocurrio un error al procesar la peticion de actualización del usuario'});
                        }else{  
                            if(!actualizado){
                                return res.status(404).send({message:'No se logro actualizar'});
                            }else{
                                return res.status(200).send({actualizado});
                            }
                        }
                    });
                }
        }
    });
}

function eliminarCasilla(req,res) {
    let params= req.body;
    let Identificacion_SIEE= params.Identificacion_SIEE;
    Casillas.find({Identificacion_SIEE:Identificacion_SIEE},(error,casilla)=>{
        if(error){
            return res.status(404).send({message:'Ocurrio un error al procesar su petición', error:error});
        }else{
            if(!casilla){
                return res.status(404).send({message:'No se encontro la casilla'});
            }else{
                //return res.status(200).send({});
                casilla
                Casillas.remove({_id:casilla._id},(error,eliminado)=>{
                    if(errorE){
                        return res.status(404).send({message:'Ocurrio un error al eliminar la casilla', error:errorE});
                    }else{
                        if(!eliminado){
                            return res.status(404).send({message:'No se encontro el elemento a eliminar'});
                        }else{
                            return res.status(200).send({CasillaEliminada:eliminado});
                        }
                    }
                });
            }
        }
    });
}
function obtenerCasilla(req,res) {
        let params = req.body;
        let Identificacion_SIEE=params.Identificacion_SIEE;
        let ProcesoEletoral= params.ProcesoElectoral;
        Casillas.findOne({Identificacion_SIEE:  Identificacion_SIEE, ProcesoElectoral:ProcesoEletoral},(error, casillaAdd)=> {
            if(error){
                    return res.status(404).send({message:'Error al procesar la peticion ',error:error});
            }else{
                if(!casillaAdd){
                    return res.status(404).send({message:'No se localizo la casilla'});
                }else{
                    return res.status(200).send({casilla:casillaAdd});
                }
            }
        });
}
function ObtenerCasillasProcesoElectoral(req,res){
    let params= req.body;
    let ProcesoElectoral= params.ProcesoElectoral;
    Casillas.find({ProcesoElectoral:ProcesoElectoral},(error,casillas)=>{
        if(error){
            return res.status(404).send({message:'Error al Obtener el listado de casillas'});
        }else{
            if(!casillas){
                return res.status(404).send({message:'No se encontraron casillas'});
            }else{
                return res.status(200).send({Casillas:casillas});
            }
        }
    });
}
module.exports={
        altaCasilla,
        asignaCasillaUsuarioAPP,
        eliminarCasilla,
        obtenerCasilla,
        ObtenerCasillasProcesoElectoral
}