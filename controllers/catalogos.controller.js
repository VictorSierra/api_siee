"use strict";
const crypto = require('crypto');
const hash = crypto.createHash('sha256');
let municipios = require('../models/Eleccion/Municipios.model');
let distritos = require('../models/Eleccion/DistritosElectorales.model');
let secciones = require('../models/Eleccion/SeccionesElectorales.model');
let procesoelectoral = require('../models/Eleccion/ProcesoElectoral.model');
let casillas = require('../models/Casillas/casillas.model');
let usuariosapp = require('../models/usersapp');
let tipoEleccion = require('../models/Eleccion/TipoEleccion.model');
let identidadTipoActas=require('../models/Eleccion/IdentidadTipoActas.models')
let casillasxUsuario=require('../models/Eleccion/CasillasUsuarios.model');
function AgregarProcesoElectoral(req, res) {
  let params = JSON.parse(req.body.datos);
  let procesoelectorall = params.ProcesoElectoral_Descripcion;
  let idproceso = params.ProcesoElectoral_ID;
  let TipoProcesoElectoral_ID = params.TipoProcesoElectoral_ID;
  let TipoProcesoElectoral_Nombre = params.TipoProcesoElectoral_Nombre;
  let TipoProcesoElectoral_Abreviacion =
    params.TipoProcesoElectoral_Abreviacion;
  let ProcesoElectoral_FechaJornada = params.ProcesoElectoral_FechaJornada;
  let ProcesoElectoral_FechaCierre = params.ProcesoElectoral_FechaCierre;
  let Activo = true;
  let nuevoProceso = new procesoelectoral();
  nuevoProceso.NombreProcesoElectoral = params.procesoelectorall;
  nuevoProceso.ProcesoElectoralID = idproceso;
  nuevoProceso.TipoProcesoElectoral = TipoProcesoElectoral_Nombre;
  nuevoProceso.TipoProcesoElectoral_ID = TipoProcesoElectoral_ID;
  nuevoProceso.TipoProcesoElectoralAbreviacion = TipoProcesoElectoral_Abreviacion;
  nuevoProceso.FechaJornada = ProcesoElectoral_FechaJornada;
  nuevoProceso.FechaCierre = ProcesoElectoral_FechaCierre;
  nuevoProceso.Activo = Activo;
  console.log(nuevoProceso);
  nuevoProceso.save((error, proceso) => {
    console.log(proceso);
    if (error) {
      return res.status(500).send({
        message: "Error no se logro guardar el proceso electoral activo " + error
      });
    } else {
      if (!proceso) {
        return res
          .status(400)
          .send({
            message: "No se guardo el proceso electoral"
          });
      } else {
        return res.status(200).send({
          proceso: proceso
        });
      }
    }
  });
}

function agregarDistritos(req, res) {
  let params = req.body;

  let distritosL = JSON.parse(params.datos);
  obtenerArregloDistritos(distritosL).then(datos => {
    distritos.insertMany(datos, (error, distritog) => {
      if (error) {
        return res
          .status(500)
          .send({
            message: "Error al agregar un distrito " + error
          });
      } else {
        if (!distritog) {
          return res.status(400).send({
            message: "No se guardo el distrito"
          });
        } else {
          return res.status(200).send({
            distrito: distritog
          });
        }
      }
    });
  });
}

async function obtenerArregloDistritos(distritosl) {
  let arreglo = [];
  for (let index = 0; index < distritosl.length; index++) {
    const element = distritosl[index];
    await consultamunicipio({
      MunicipioElectoral_ID: element.MunicipioElectoral_ID
    }).then(resultado => {
      arreglo.push({
        DistritoElectoral_ID: element.DistritoElectoral_ID,
        Numero: element.DistritoElectoral_Numero,
        Descripcion: element.DistritoElectoral_Descripcion,
        Abreviacion: element.DistritoElectoral_Abreviacion,
        MunicipioElectoral_ID: element.MunicipioElectoral_ID,
        Municipio: resultado._id,
        Activo: true
      });
    });
  }

  return arreglo;
}

function agregarSecciones(req, res) {
  let params = req.body;
  let seccionesL = JSON.parse(params.datos);
  obtenerArregloSecciones(seccionesL).then(resultado => {
    secciones.insertMany(resultado, (error, seccion) => {
      if (error) {
        return res
          .status(500)
          .send({
            message: "Error al agregar una seccion electoral " + error
          });
      } else {
        if (!seccion) {
          return res.status(400).send({
            message: "No se agrego la sección"
          });
        } else {
          return res.status(200).send({
            seccionguardada: seccion
          });
        }
      }
    });
  });
}
async function obtenerArregloSecciones(seccionesL) {
  let arreglo = [];
  for (let index = 0; index < seccionesL.length; index++) {
    const element = seccionesL[index];
    let mun;
    await consultamunicipio({
      MunicipioElectoral_ID: element.MunicipioElectoral_ID
    }).then(resultado => {
      mun = resultado;
    });
    await consultadistrito({
      DistritoElectoral_ID: element.DistritoElectoral_ID
    }).then(resultdist => {
      arreglo.push({
        SeccionElectoral_ID: element.SeccionElectoral_ID,
        SeccionElectoral: element.SeccionElectoral_Numero,
        Municipio: mun._id,
        DistritoElectoral: resultdist._id,
        Activo: true
      });

    });
  }
  return arreglo;
}

function agregarMunicipios(req, res) {
  let params = req.body;
  let municipiosL = JSON.parse(params.datos);
  let arreglo = [];
  municipiosL.forEach(element => {
    arreglo.push({
      NombreMunicipio: element.Municipio_Nombre,
      MunicipioElectoral_ID: element.MunicipioElectoral_ID,
      Municipio_ID: element.Municipio_ID,
      NumeroMunicipio: element.Municipio_Numero,
      Abreviacion: element.Abreviado,
      Activo: true
    });
  });
  //let municipio = new municipios();
  municipios.insertMany(arreglo, (error, MunicipioAgregado) => {
    if (error) {
      return res
        .status(500)
        .send({
          message: "Error al agregar un municipio " + error
        });
    } else {
      if (!MunicipioAgregado) {
        return res.status(400).send({
          message: "No se guardo el municipio"
        });
      } else {
        return res.status(200).send({
          Municipio: MunicipioAgregado
        });
      }
    }
  });
  // municipios.save();
}



function agregarCasillas(req, res) {
  let params = req.body;
  let casillasL = JSON.parse(params.datos);
  obtenerArregloCasillas(casillasL).then(resultados => {
    casillas.insertMany(resultados, (error, casillasres) => {
      if (error) {
        return res
          .status(500)
          .send({
            message: "Error al agregar casillas " + error
          });
      } else {
        if (!casillasres) {
          return res.status(400).send({
            message: "No se guardaron las casillas"
          });
        } else {
          return res.status(200).send({
            casillas: casillasres
          });
        }
      }
    });
  });
}

async function obtenerArregloCasillas(casillasL) {
  let arreglo = [];
  let proceso = await consultaprocesoelectoral({
    Activo: true
  });
  for (let index = 0; index < casillasL.length; index++) {
    const element = casillasL[index];
    let mun;
    let dist;
    await consultamunicipio({
      MunicipioElectoral_ID: element.MunicipioElectoral_ID
    }).then(resultado => {
      mun = resultado;
    });
    await consultadistrito({
      DistritoElectoral_ID: element.DistritoElectoral_ID
    }).then(resultado => {
      dist = resultado;
    });
    await consultaseccion({
      SeccionElectoral_ID: element.SeccionElectoral_ID
    }).then(resultado => {
      arreglo.push({
        ProcesoElectoral: proceso._id,
        IdentificacionCasilla: {
          Entidad_Federativa: 27,
          Distrito_Electoral: dist.Numero,
          Seccion_Electoral: resultado.SeccionElectoral,
          Tipo_Casilla: element.Casilla_Clave,
          Municipo: mun.NumeroMunicipio
        },
        Municipio: mun._id,
        Distrito: dist._id,
        Seccion: resultado._id,
        Identificacion_SIEE: {
          SeccionElectoral_ID: element.SeccionElectoral_ID,
          SeccionElectoral_Numero: element.SeccionElectoral_Numero,
          DistritoElectoral_ID: element.DistritoElectoral_ID,
          DescripcionCasilla: element.Casilla_Descripcion,
          Municipio_Numero: mun.NumeroMunicipio,
          MunicipioElectoral_ID: mun.MunicipioElectoral_ID,
          ClaveCasilla: element.Casilla_Clave
        }
      });
    });

  }
  return arreglo;
}

//Consulta por medio de promesa
function consultamunicipio(consulta) {
  return new Promise(resolve => {
    let query = municipios.where(consulta);
    query.findOne(function (error, municipio) {
      if (error) resolve(null);
      if (municipio) resolve(municipio);
      if (!municipio) resolve(municipio);
    });
  });
}
//consulta por medio de promesa
function consultadistrito(consulta) {
  return new Promise(resolve => {
    let query = distritos.where(consulta);
    query.findOne(function (error, distrito) {
      if (error) return resolve(null);
      if (distrito) return resolve(distrito);
      if (!distrito) return resolve(null);
    });
  });
}

function consultaprocesoelectoral(consulta) {
  return new Promise(resolve => {
    let query = procesoelectoral.where(consulta);
    query.findOne(function (error, proceso) {
      if (error) return resolve(null);
      if (proceso) return resolve(proceso);
      if (!proceso) return resolve(null);
    });
  });
}

function consultaseccion(consulta) {
  return new Promise(resolve => {
    let query = secciones.where(consulta);
    query.findOne(function (error, seccion) {
      if (error) return resolve(null);
      if (seccion) return resolve(seccion);
      if (!seccion) return resolve(null);
    });
  });
}




function relacionarSeccionconDistrito(req, res) {
  //obtener Distritos
  //buscar secciones del distrito
  secciones.find({}).exec().then((resultados) => {
    if (!resultados) {
      return res.status(400).send({
        message: 'No se encontraron secciones electorales'
      });
    } else {
      let result = procesasecciones(resultados);
      return res.status(200).send({
        Secciones: result
      });
    }
  });
}

async function procesasecciones(resultados) {
  for (let index = 0; index < resultados.length; index++) {
    const element = resultados[index];
    await insertarSeccionenDistrito(element.DistritoElectoral, element._id).then((resultado) => {
      //      console.log(resultado);
    });
  }
  return resultados;
}

function insertarSeccionenDistrito(Distrito_id, idSeccion) {
  return new Promise(resolve => {
    distritos.findById(Distrito_id, async (error, dist) => {
      if (error) {
        return resolve(null);
      } else {
        if (!dist) {
          return resolve(null);
        } else {
          dist.Secciones.push({
            _id: idSeccion
          });
          await dist.save((error, guardado) => {
            if (error) {
              return resolve(null);
            } else {
              if (!guardado) {
                return resolve(null);
              } else {
                return resolve(dist);
              }
            }
          });
        }
      }
    });
  });
}

function relacionseccionescasillas(req, res) {
  secciones.find({}).then(resultado => {
    guardaresultados(resultado);
    res.status(200).send({
      message: 'Termino de relacionar los datos'
    });
  });
}

async function guardaresultados(resultado) {
  for (let index = 0; index < resultado.length; index++) {
    const element = resultado[index];
    let arreglo = await ObtenerCasillasenSeccion(element._id);
    if (arreglo != null) {
      for (let i = 0; i < arreglo.length; i++) {
        const casilla = arreglo[i];
        element.Casillas.push({
          idCasilla: casilla._id
        });
      }
      await element.save((error, guardados) => {
        if (error) {} else {
          if (!guardados) {} else {
            console.log(guardados);
          }
        }
      });
    }
  }
}

function ObtenerCasillasenSeccion(seccionID) {
  return new Promise(resolve => {
    casillas.find({
      Seccion: seccionID
    }).exec(function (error, docs) {
      if (error) {
        return resolve(null);
      } else {
        if (!docs) {
          return resolve(null);
        } else {
          return resolve(docs);
        }
      }
    });
  });
}

function encriptarpass(element) {
  let hash = crypto.createHash('sha256');
  let cadena = hash.update(element).digest('hex');
  return cadena;
}

function RegistrarUsuariosActivosAPP(req, res) {
  let params = req.body;
  console.log(params.datos);
  let usuarios = JSON.parse(params.datos);
  usuariosapp.insertMany(ObtenerArregloUsuarios(usuarios), (error, resultados) => {
    if (error) {
      return res.status(500).send({
        message: 'Error al insertar los usuarios de app ' + error
      });
    } else {
      if (!resultados) {
        return res.status(400).send({
          message: 'No se insertaron los usuarios de la app'
        });
      } else {
        return res.status(200).send({
          usuariosapp: resultados
        });
      }
    }
  })
}

function ObtenerArregloUsuarios(usuarios) {
  let arreglo = [];
  for (let index = 0; index < usuarios.length; index++) {
    const element = usuarios[index];
    arreglo.push({
      nombre: element.nombre,
      email: '',
      password: encriptarpass(element.pass),
      fecha_creacion: Date.now(),
      rol: element.rol,
      activo: element.usuarioActivoSIIE,
      activoSIEE: element.usuarioActivoSIIE,
      IdUsuarioSIEAPP: element.Usuario_ID,
      codigoImei: element.imei,
      UIDD: '',
      estatus: 'Alta',
      Sincronizar: true
    });
    hash.end();
  }
  return arreglo;
}

function agregarTipoElecciones(req,res) {
  let params = req.body;
  let datos=JSON.parse(params.datos);    
  let arreglo=ObtenerArregloTipoEleccion(datos)  
  tipoEleccion.insertMany(arreglo, (error,tipoeleccion)=>{
   if(error){
      return res.status(500).send({message:'Error al insertar los tipos de elecciones ' +error});
   }else{
      if(!tipoeleccion){
          return res.status(400).send({message:'No se insertaron los tipos de elección'});
         }else{
            return res.status(200).send({tipoelecciones:tipoeleccion});
         }
   }
  });
}

function ObtenerArregloTipoEleccion(datos) {
  let arreglo = [];    
  for (let index = 0; index < datos.length; index++) {
    const element = datos[index];
    
    arreglo.push({
      TipoEleccion_ID: element.TipoEleccion_ID,
      Nombre: element.TipoEleccion_Nombre,
      Abreviacion: element.TipoEleccion_Abreviacion,
      Tipoeleccion_Activo: element.Tipoeleccion_Activo,
      ProcesoElectoral: element.ProcesoElectoral_ID
    });
  }  
  
  return arreglo;
}

function AgregarIdentidadTipoActas(req,res){
    let params=req.body;
    let datos=JSON.parse(params.datos);
    
    let arreglo=obtenerArregloIdentidadTipoActas(datos);
    identidadTipoActas.insertMany(arreglo,(error,identidadTipoActas)=>{
     if(error){
        return res.status(500).send({message:'Error al procesar la petición de inserción de Identidad Tipo Actas ' +error});
     }else{
          if(!identidadTipoActas){
            return res.status(400).send({message:'No se Inertaron los registro de Identidad Tipo Acta'});
           }else{
                return res.status(200).send({resultado:identidadTipoActas});
           }
     }
    });
}
function obtenerArregloIdentidadTipoActas(datos){
  let arreglo = [];    
  for (let index = 0; index < datos.length; index++) {
    const element = datos[index];
    
    arreglo.push({
      TipoEleccion_ID: element.TipoEleccion_ID,
      TipoCasilla_ID: element.TipoCasilla_ID,
      FoliacionActa_Numero: element.FoliacionActa_Numero,
      ProcesoElectoral_ID: element.ProcesoElectoral_ID,      
    }); 
  }
  return arreglo;
}
function AgregarCasillasxUsuario(req,res){
  let params= req.body;
  let datos=JSON.parse(params.datos);
  console.log(datos);
  let arreglo=obtenerArregloCasillasUsuarios(datos);
  casillasxUsuario.insertMany(arreglo,(error,resultados)=>{
   if(error){
      return res.status(500).send({message:'Error al ingresar las casillas x usuario ' +error});
   }else{
        if(!resultados){
          return res.status(400).send({message:'no se ingresaron las casillas x usuario'});
         }else{
              return res.status(200).send({resultados:resultados});
         }
   }
  }) 
}

 function obtenerArregloCasillasUsuarios(registros) {
  let arreglo = [];
  for (let index = 0; index < registros.length; index++) {
    const element = registros[index];   
    console.log(element);
      arreglo.push({
        ProcesoElectoral_ID:element.ProcesoElectoral_ID,
        Casilla_Descripcion:element.Casilla_Descripcion,
        Casilla_Clave:element.Casilla_Clave,
        SeccionElectoral_Numero:element.SeccionElectoral_Numero,
        DistritoElectoral_ID:element.DistritoElectoral_ID,
        MunicipioElectoral_ID:element.MunicipioElectoral_ID,
        TipoEleccion_ID:element.TipoEleccion_ID,
        FoliacionActa_Numero:element.FoliacionActa_Numero,
        Eleccion_ID:element.Eleccion_ID,
        Usuario_ID:element.Usuario_ID       
      });
  
  }

  return arreglo;
}
module.exports = {
  AgregarProcesoElectoral,
  agregarDistritos,
  agregarSecciones,
  agregarMunicipios,
  agregarCasillas,
  relacionarSeccionconDistrito,
  relacionseccionescasillas,
  RegistrarUsuariosActivosAPP,
  agregarTipoElecciones,
  AgregarIdentidadTipoActas,
  AgregarCasillasxUsuario
}