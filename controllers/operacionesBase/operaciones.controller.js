'use strict'
let usersApp= require('../../models/usersapp');
let casillas=require('../../models/Casillas/casillas.model');
let usuariosxcasilla=require('../../models/Eleccion/CasillasUsuarios.model');
let distritoselectorales=require('../../models/Eleccion/DistritosElectorales.model');
let municipioselectorales=require('../../models/Eleccion/Municipios.model');
let identidadactas=require('../../models/Eleccion/IdentidadTipoActas.models');
let procesoelectoral=require('../../models/Eleccion/ProcesoElectoral.model');
let seccionelectoral=require('../../models/Eleccion/SeccionesElectorales.model');
let tipoeleccion=require('../../models/Eleccion/TipoEleccion.model');
function limpiarescenario(req,res){
    usersApp.deleteMany().then(()=>{
        casillas.deleteMany().then(()=>{
            usuariosxcasilla.deleteMany(()=>{
                distritoselectorales.deleteMany(()=>{
                    municipioselectorales.deleteMany(()=>{
                        identidadactas.deleteMany(()=>{
                            procesoelectoral.deleteMany(()=>{
                                seccionelectoral.deleteMany(()=>{
                                    tipoeleccion.deleteMany(()=>{
                                        return res.status(200).send({message:'vaciado'});
                                    });
                                });
                            });
                        });
                    });
                });
            });
        });
    });
                                
    
}
function borrarTodosUsuariosAPP(req,res){
        //appusers
        usersApp.deleteMany({},(error,elementoseliminados)=>{
         if(error){
            return res.status(500).send({message:'Error al eliminar los usuarios app ' +error});
         }else{
              if(!elementoseliminados){
                return res.status(400).send({message:'No se eliminaron usuarios app'});
               }else{
                    return res.status(200).send({eliminados:elementoseliminados});
               }
         }
        });
}
function borrarCasillas(req,res){
    casillas.deleteMany({},(error,casillaseliminadas)=>{
     if(error){
        return res.status(500).send({message:'Error al eliminar las casillas ' +error});
     }else{
          if(!casillaseliminadas){
            return res.status(400).send({message:'No se eliminaron las casillas'});
           }else{
                return res.status(200).send({eliminadas:casillaseliminadas});
           }
     }
    });
}
function borrarcasillasxusuarios(req,res){
    usuariosxcasilla.deleteMany({},(error,casillas)=>{
     if(error){
        return res.status(500).send({message:'Error al eliminar las casillas por usuario ' +error});
     }else{
          if(!casillas){
            return res.status(400).send({message:'no se eliminaron las casillas por usuario'});
           }else{
                return res.status(200).send({eliminadas:casillas});
           }
     }
    });
}
function borrarDistritos(req,res){

}
function borrarIdentidadTipoActas(req,res){

}
function borrarMunicipios(req,res){

}
function borrarSecciones(){

}
function borrarTipoeleccion(){

}


module.exports={
    borrarTodosUsuariosAPP,
    borrarCasillas,
    borrarcasillasxusuarios,
    limpiarescenario
}