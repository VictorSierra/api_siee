'use strict'
let Puesto = require('../models/puesto')

function altaPuesto(req, res) {
    var params = req.body;
    let puesto = new Puesto();
    console.log('estoy aqui');
    puesto.nombre_puesto = params.puesto.nombre_puesto;
    puesto.nemotecnico_puesto = params.puesto.nemotecnico_puesto;
    puesto.tipo_puesto = params.puesto.tipo_puesto;
    puesto.estatus = true;
    if (params.puesto.nombre_puesto != null && params.puesto.tipo_puesto != null) {
        puesto.save((err, puestonuevo) => {
            if (err) {
                return res.status(500).send({ message: 'Error al agredar un puesto nuevo' });
            } else {
                if (!puestonuevo) {
                    return res.status(400).send({ message: 'No se guardo el nuevo puesto' });
                } else {
                    res.status(200).send(puestonuevo);
                }
            }
        });
    } else {
        return res.status(200).send({ message: 'Llene los campos' });
    }
}

function edicionPuesto(req, res) {
    var params = req.body;
    var idpuesto = params.idpuesto;
    if (params.puesto.nombre_puesto != null && params.puesto.nemotecnico_puesto != null) {
        Puesto.findByIdUpdate(idpuesto, {
            nombre_puesto: params.puesto.nombre_puesto,
            nemotecnico_puesto: params.puesto.nemotecnico_puesto,
            tipo_puesto: params.puesto.tipo_puesto
        }, (err, puestoeditado) => {
            if (err) {
                return res.status(500).send({ message: 'Error al editar el puesto' });
            } else {
                if (!puestoeditado) {
                    return res.status(400).send({ message: 'No se guardaron los cambios a el puesto' });
                } else {
                    res.status(200).send(puestoeditado);
                }
            }
        });
    } else {
        return res.status(200).send({ message: 'Rellene los campos' });
    }
}

function eliminarPuesto(req, res) {
    var id = req.body.id;
    Puesto.findByIdAndRemove(id, (err, puestoEliminado) => {
        if (err) {
            return res.status(500).send({ message: 'Error al eliminar el puesto' });
        } else {
            if (!puestoEliminado) {
                return res.status(400).send({ message: 'No se elimino el puesto' });
            } else {
                res.status(200).send(puestoEliminado);
            }
        }
    });
}

function desactivarPuesto(req, res) {
    var params = req.body;
    var idpuesto = params.idpuesto;

    Puesto.findByIdUpdate(idpuesto, { estatus: false }, (err, puestoeditado) => {
        if (err) {
            return res.status(500).send({ message: 'Error al editar el puesto' });
        } else {
            if (!puestoeditado) {
                return res.status(400).send({ message: 'No se guardaron los cambios a el puesto' });
            } else {
                res.status(200).send(puestoeditado);
            }
        }
    });
}

function activarPuesto(req, res) {
    var params = req.body;
    var idpuesto = params.idpuesto;

    Puesto.findByIdUpdate(idpuesto, { estatus: true }, (err, puestoeditado) => {
        if (err) {
            return res.status(500).send({ message: 'Error al editar el puesto' });
        } else {
            if (!puestoeditado) {
                return res.status(400).send({ message: 'No se guardaron los cambios a el puesto' });
            } else {
                res.status(200).send(puestoeditado);
            }
        }
    });
}

function buscarPuesto(req, res) {

}

function listadoPuesto(req, res) {
    var page = req.params.page;
    var itemsporpagina = 10;
    Puesto.find().sort('nombre_puesto').paginate(page, itemsporpagina, function(err, puestos, total) {
        if (err) {
            return res.status(500).send({ message: 'Error al obtener el listado de puestos' });
        } else {
            if (!puestos) {
                return res.status(400).send({ message: 'No se logro obtener el listado de puestos' });
            } else {
                res.status(200).send({ puestos });
            }
        }
    });
}
module.exports = {
    altaPuesto,
    edicionPuesto,
    eliminarPuesto,
    buscarPuesto,
    listadoPuesto
}