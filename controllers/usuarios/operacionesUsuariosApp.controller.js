'use strict'
let bcrypt = require('bcrypt-nodejs');
let jwt = require('../services/jwt');
let UsuarioAPP=require('../../models/usersapp');
const crypto = require('crypto');
const hash = crypto.createHash('sha256');

function cambiaPassword(req,res){
    let params=req.body;
    let IdUsuarioSIEAPP=params.IdUsuarioSIEAPP;
    let password=params.nuevopassword;
    UsuarioAPP.findOne({'IdUsuarioSIEAPP':IdUsuarioSIEAPP},(error,usuario)=>{
        if(error){
            return res.status(404).send({message:'Ocurrio un error en la petición',error:error});
        }else{
            if(!usuario){
                return res.status(404).send({message:'No se encontro al usuario'});
            }else{
                bcrypt.hash(params.dato.password, null, null, function(error, hash) {
                    if(error){
                        return res.status(404).send({message:'Error al encriptar la contraseña'});
                    }else{
                        if(!hash){
                            return res.status(404).send({message:'No se pudo generar el hash'});
                        }else{
                            usuario.password=hash;
                            UsuarioAPP.save((error,UActualizado)=>{
                                if(error){
                                    return res.status(404).send({message:'Error al guardar la contraseña del usuario'});
                                }else{
                                    if(!UActualizado){
                                        return res.status(404).send({message:'No se actualizo la contraseña del usuario'});
                                    }else{  
                                        return res.status(200).send({usuariomodificado:UActualizado.IdUsuarioSIEAPP});
                                    }
                                }
                            });
                        }
                    }                    
                } );                               
            }
        }
    });
}

function desactivarUsuarioAPP(req,res){
    let params= req.body;
    let IdUsuarioSIEAPP=params.IdUsuarioSIEAPP;
    UsuarioAPP.update({ IdUsuarioSIEAPP: IdUsuarioSIEAPP }, { $set: { activo: false }}, (error,usuario)=>{
        if(error){
            return res.status(404).send({message:'Error al procesar la petición',error:error});
        }else{
            if(!usuario){
                    return res.status(404).send({message:'No se encontro el usuario'});
            }else{
                return  res.status(200).send ({usuario:usuario});
            }
        }
    });
}
function activarUsuarioAPP(req,res){
    let params= req.body;
    let IdUsuarioSIEAPP=params.IdUsuarioSIEAPP;
    UsuarioAPP.update({ IdUsuarioSIEAPP: IdUsuarioSIEAPP }, { $set: { activo: true }}, (error,usuario)=>{
        if(error){
            return res.status(404).send({message:'Error al procesar la petición',error:error});
        }else{
            if(!usuario){
                    return res.status(404).send({message:'No se encontro el usuario'});
            }else{
                return  res.status(200).send ({usuario:usuario});
            }
        }
    });
}

function altaUsuarioAPP(req, res) {
    let usuario = new UsuarioAPP();
    let params = req.body;
    usuario.Nombre = params.dato.nombre;    
    usuario.rol = params.dato.rol;        
    usuario.activo = params.dato.activo;
    usuario.IdUsuarioSIEAPP= params.dato.IdUsuarioSIEAPP;
    if (params.dato.password) {
        bcrypt.hash(params.dato.password, null, null, function(err, hash) {
            usuario.password = hash;
            if (usuario.nombre != null && usuario.IdUsuarioSIEAPP!=null&& usuario.email != null && usuario.rol != null) {
                usuario.save((err, usuarioStored) => {
                    if (err) {
                        return res.status(500).send({ message: 'Error al registrar el usuario' });
                    } else {
                        if (!usuarioStored) {
                            return res.status(400).send({ message: 'No se ha registrado el usuario' });
                        } else {
                         return res.status(200).send({ usuario: usuarioStored });
                        }
                    }
                });
            } else {
                return res.status(200).send({ message: 'Introdusca todos los campos' });
            }
        });
    } else {
        return res.status(500).send({ message: 'Introdusca password' });
    }
}

module.exports={
    cambiaPassword,
    desactivarUsuarioAPP,
    activarUsuarioAPP,
    altaUsuarioAPP
}