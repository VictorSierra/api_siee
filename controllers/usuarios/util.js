'use strict'
let UsuarioAPP = require('../../models/usersapp');
let IdentidadTipoActas = require('../../models/Eleccion/IdentidadTipoActas.models');
let Global = require('../../global');
let request = require('request-promise');
const image2base64 = require('image-to-base64');


function enviaImage(dato) {
    let notificacion = JSON.parse(dato);
    let Usuario_ID = notificacion.Usuario_ID;
    let fechahora = String(notificacion.horacaptura).split('|');
    let fecha = fechahora[0];
    let hora = fechahora[1];
    let hash = notificacion.hash;
    let qr = notificacion.qr;
    let datos = String(qr).split('|');
    let estado = datos[0];
    let tipoIdentificacionActa = datos[1];
    let distrito = datos[3];
    let seccion = datos[4];
    let claveCasilla = datos[5];
    let nombreArchivo = notificacion.nombreArchivoRepositorio;

    IdentidadTipoActas.findOne({
        FoliacionActa_Numero: tipoIdentificacionActa
    }, (error, resultadoTipoactas) => {
        if (error) {
            //  return res.status(500).send({
            console.log('Error al consultar el tipo de elección ' + error);
            //  });
        } else {
            if (!resultadoTipoactas) {
                //   return res.status(400).send({
                console.log('No se encontro el tipo de elección');
                //     });
            } else {
                let tipoeleccion = resultadoTipoactas.TipoEleccion_ID;
                UsuarioAPP.findById(Usuario_ID, (error, usuario) => {
                    if (error) {
                        
                    } else {
                        if (!usuario) {
                           
                        } else {
                            image2base64(Global.Global.urlarchivo + "/archivo/" + nombreArchivo)
                                .then(
                                    (response) => {
                                        request.post({
                                            url: "http://localhost:6002/api/" + 'obtenerEleccion',
                                            body: {
                                                seccion: Number.parseInt(datos[4]),
                                                tipoeleccion: tipoeleccion, //ObtenerTipoeleccion(tipoIdentificacionActa).TipoEleccion_ID,
                                                clave_casilla: datos[5]
                                            },
                                            json: true
                                        }).then(function(Eleccion_ID) {


                                            request.post({
                                                url: Global.Global.urlsieeactas,
                                                formData: {
                                                    file: response,
                                                    Eleccion_ID: Eleccion_ID.Eleccion_ID[0].Eleccion_ID,
                                                    ProcesoElectoral_ID: 'ORD-2018',
                                                    Fecha_Acopio: fecha,
                                                    Hora_Acopio: hora,
                                                    Usuario_ID: usuario.IdUsuarioSIEAPP,
                                                    Hash: hash
                                                }
                                            }).then(function(htmlstring) {
                                                console.log(htmlstring);

                                            }).catch(function(error) {
                                                console.log(error);
                                            });

                                        });
                                    }
                                )
                                .catch(
                                    (error) => {
                                        console.log(error); //Exepection error....
                                    }
                                );

                        }
                    }
                });
            }
        }
    });

}

function enviaImagesinqr(dato) {
    let notificacion = JSON.parse(dato);
    console.log(notificacion);
    console.log('sin qr');
    const canalsqr = 'Actassinqr';
    let Usuario = notificacion.Usuario;
    let Eleccion = notificacion.Eleccion;
    let Ambito = notificacion.Ambito;
    let Seccion = notificacion.Seccion;
    let Casilla = notificacion.Casilla;
    let Proceso_Electoral = notificacion.Proceso_Electoral;
    let QR_Acta = notificacion.QR_Acta;
    let Ubicacion = notificacion.Ubicacion;
    let Imei = notificacion.Imei;
    
    let fechahora = String(notificacion.horacaptura).split(' ');
    console.log(fechahora);
    let fecha = fechahora[0];
    let hora = fechahora[1];
    let hash = notificacion.hash;
    let nombreArchivo = notificacion.nombreArchivoRepositorio;
    console.log(Global.Global.urlarchivo + "/archivo/" + nombreArchivo);
    image2base64(Global.Global.urlarchivo + "/archivo/" + nombreArchivo)
        .then((response) => {

            // const opcionesDeSolicitud = {
            //     method: 'POST',
            //     uri: "https://gorest.co.in/public/v2/users",
            //     headers: {
            //       'Content-Type': 'application/json',
            //       'Authorization': `Bearer 4153f87ead4db2d02d383fdb6a1c59e9876c91ac01592543cc1104169ceb3814`, // Agrega el token de acceso aquí
            //     },
            //     body: {
            //         "name": "Juan Perez",
            //         "gender": "male",
            //         "email": "juan.perez@gmail.com",
            //         "status": "active"
            //     },
            //     json: true, // Indica que los datos del cuerpo se enviarán como JSON
            //   };

            // request.post(opcionesDeSolicitud).then(function(res) {
            //     console.log('respondiendo...', res);               

            // }).catch(function(error) {
            //     console.log(error);
            // });
            
            //console.log(response);

            request.post({
                url: Global.Global.urlsieeactas,
                
                formData: {
                    file: response,
                    Eleccion: Eleccion,
                    Ambito: Ambito,
                    Seccion: Seccion,
                    Casilla: Casilla,
                    ProcesoElectoral: Proceso_Electoral,
                    Fecha_Acopio: fecha,
                    Hora_Acopio: hora,
                    Usuario: Usuario,
                    Hash: hash,
                    QR_Acta: QR_Acta,
                    Ubicacion: Ubicacion,
                    Imei: Imei

                }
            }).then(function(htmlstring) {
                console.log(htmlstring);
                let resultado_del_envio = JSON.parse(htmlstring);

                if (resultado_del_envio.success == true) {
                    console.log("El acta se envio correctamente");
                    //se puede actilizar el registro informando que ya recibio el archivo el siee
                    //registros.registraActa(notificacion);
                }
            }).catch(function(error) {
                console.log(error);
            });
        })
        .catch(
            (error) => {
                console.log(error); //Exepection error....
            }
        );
}

module.exports = {
    enviaImage,
    enviaImagesinqr
}