'use strict'
var Global = require('./global');
//var assert = require('assert');
var mongoose = require('mongoose');
var app = require('./app');
var server = require('http').createServer()

let fs = require('fs');
let path = require('path');
let utilidad = require('./controllers/usuarios/util');

//var io= require('socket.io').listen(server);
var operaciones = require('./controllers/casillas/operacionesCasillas.controller');

server.listen(6001);
//var server=require('http').Server(app);
let redis = require('redis');

//jemanuel descomentar 
let redisClient = redis.createClient(Global.Global.urlredis);



var port = process.env.PORT || 6000
app.listen(port, function() {
    console.log('Servidor de api rest escuchando en el puerto: ' + port);

});


mongoose.connect(Global.Global.urlBase, { useMongoClient: true, autoIndex: false });
mongoose.Promise = global.Promise;
var db = mongoose.connection;
db.on('error',
    console.error.bind(console, 'error de conexion')
);
db.once('open', function() {
    console.log('la base de datos se conecto correctamente...');


});
const canal = 'ActasEnviadas';
//const canalsiee='actaRecibida';
redisClient.on('subscribe', function(channel, count) {

    console.log(count);
    console.log(channel);
});
const canalsiqr = 'Actassinqr';
redisClient.on('message', function(canal1, mensaje) {
    //console.log(mensaje);
    console.log("Canal: " + canal1);
    if (canal1 == canalsiqr) {
        console.log('sin qr');
        console.log(mensaje);
        utilidad.enviaImagesinqr(mensaje);
        //redisClientsiie.publish('actasinqr_',mensaje);  
    } else if (canal1 == canal) {
        console.log('con qr');
        console.log(mensaje);
        // io.emit('hello',{data:mensaje});
        utilidad.enviaImage(mensaje);
        //  redisClientsiie.publish(canalsiee,mensaje);  
    }

});

redisClient.subscribe(canal);
redisClient.subscribe(canalsiqr);
// io.on('connection', function(socket) {
//     console.log('a user connected');
//     socket.emit('hello', 'HOLA');
// });