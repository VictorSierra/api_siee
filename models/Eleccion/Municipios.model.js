'use strict'
let mongoose=require('mongoose');
let Schema= mongoose.Schema;
let MunicipiosSchema=Schema({    
    NombreMunicipio:String,
    NumeroMunicipio:Number,
    Abreviacion:String,
    Municipio_ID:Number,
    MunicipioElectoral_ID:Number,
    Activo:Boolean    
});

module.exports = mongoose.model('Municipio',MunicipiosSchema);