'use strict'
let mongoose=require('mongoose');
let Schema=mongoose.Schema;
let RegistroActasSchema=Schema({
    nombreUsusario:String,
    idUsuario:String,
    Usuario_ID:String,
    Eleccion_ID:String,
    Tipo_Eleccion_ID:String,
    Proceso_Electoral:String,                                    
    horaProcesadoServidor:Date,
    horacaptura:String,
    hash:String,
    qr:String,                                
    nombreArchivoRepositorio:String,
    ArchivoOrigen:String        
});

module.exports=mongoose.model('RegistroRecepcionActas',RegistroActasSchema);