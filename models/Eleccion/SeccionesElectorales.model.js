'use strict'
let mongoose=require('mongoose');
let Schema=mongoose.Schema;
let SeccionesSchema=Schema({
    SeccionElectoral_ID:Number,
    SeccionElectoral:Number,
    DistritoElectoral:{ type: Schema.Types.ObjectId, ref: 'distritos', index:true },
    Municipio:{ type: Schema.Types.ObjectId, ref: 'municipios', index:true },
    Activo:Boolean,
    Casillas:[{
        idCasilla:{type:Schema.Types.ObjectId,ref: 'casillas', index:true }
    }]
},{ usePushEach: true });
module.exports = mongoose.model('Seccion', SeccionesSchema);