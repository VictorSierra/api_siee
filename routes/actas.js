'use strict'
let express= require('express');
let ActasController=require('../controllers/actas/operacionesActasPREP.controller');
let api=express.Router();
var md_auth = require('../middlewares/autenticacion');


/*
    Alta de Actas,    
    Obtener Actas Capturadas.
    Obtener Actas,
    Obtener Acta,
    eliminar Acta?,
    extraer Actas Subidas,
    obtener imagen acta,
    
*/ 

api.post('/obtener-acta',ActasController.obtenerActa);
api.post('/agregar-acta',ActasController.AgregarActaDesdeSIEE);


module.exports=api;