'use strict'
let express= require('express');
let CasillaController=require('../controllers/casillas/operacionesCasillas.controller');
let api=express.Router();
var md_auth = require('../middlewares/autenticacion');


/*
alta casilla
consulta casilla
obtiene todas las casillas
asigna usuario app
    
*/ 

api.post('/alta-casilla', CasillaController.altaCasilla);//
api.post('/asigna-Casilla-Usuario', CasillaController.asignaCasillaUsuarioAPP);
api.post('/eliminar-Casilla', CasillaController.eliminarCasilla);
api.post('/obtener-casilla', CasillaController.obtenerCasilla);
api.post('/obtener-casillas-proceso-electoral', CasillaController.ObtenerCasillasProcesoElectoral);

module.exports=api;