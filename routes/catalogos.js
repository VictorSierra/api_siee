'use strict'
let express= require('express');
let api=express.Router();
let md_auth = require('../middlewares/autenticacion');
let CatalogosController=require('../controllers/catalogos.controller');
let OperacionesController=require('../controllers/operacionesBase/operaciones.controller');
api.post('/agregarprocesoelectoral',CatalogosController.AgregarProcesoElectoral);
api.post('/agregarmunicipios',CatalogosController.agregarMunicipios);
api.post('/agregardistritos',CatalogosController.agregarDistritos);
api.post('/agregarsecciones',CatalogosController.agregarSecciones);
api.post('/agregaracciones',CatalogosController.agregarCasillas);
api.post('/relacionarseccioncondistrito',CatalogosController.relacionarSeccionconDistrito);
api.post('/relacionaseccioncasillas',CatalogosController.relacionseccionescasillas);
api.post('/obtenerusuariosini',CatalogosController.RegistrarUsuariosActivosAPP);
api.post('/agregartipoelecciones',CatalogosController.agregarTipoElecciones);
api.post('/agregaridentidadtipoactas',CatalogosController.AgregarIdentidadTipoActas);
api.post('/agregarcasillasxusuario', CatalogosController.AgregarCasillasxUsuario);


api.post('/eliminarusuariosapp',OperacionesController.borrarTodosUsuariosAPP);
api.post('/eliminarcasillasxusuario',OperacionesController.borrarcasillasxusuarios);
api.post('/eliminarcasillas',OperacionesController.borrarCasillas);
api.post('/vaciarbase',OperacionesController.limpiarescenario);
module.exports=api;