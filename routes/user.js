'use strict'

var express = require('express');
var UserController = require('../controllers/user');
var api = express.Router();
var md_auth = require('../middlewares/autenticacion');



api.post('/login-user', UserController.loginUsuario);
// api.get('/altausuarioini', UserController.altaUsuarioIni);
// api.post('/altausuario', UserController.altaUsuario);
api.post('/altausuario_movil', UserController.alta_usuario_movil);
api.post('/eliminarsuario_movil', UserController.eliminar_usuario_movil);
api.post('/folios-activacion', UserController.prep_ingreso_de_folios_de_activacion);
api.post('/desactivar-folios', UserController.desactivar_folios_de_activacion);
api.post('/consulta-folios', UserController.consultar_folios_de_activacion);

module.exports = api;