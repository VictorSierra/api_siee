'user strict'
var jwt = require('jwt-simple');
var moment = require('moment');

var secret = 'clave_secreta_pagina';
exports.createToken = function(user) {
    var payload = {
        sid: user._id,
        nombre: user.nombre,
        email: user.email,
        fecha_creacion: user.fecha_creacion,
        role: user.role,
        iat: moment().format(),        
        exp: moment().add(2, 'd').format()
    };
    return jwt.encode(payload, secret);
}